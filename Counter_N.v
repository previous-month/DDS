module Counter_N (
    input clk,       // 输入时钟信号
    input rst,       // 复位信号
    input [2:0] N,   // 计数值
    output reg [2:0] cnt // 4位计数器输出
);

    always @(posedge clk or posedge rst) begin
        if (rst) // 如果复位信号为1
            cnt <= 4'd0; //计数值清零
        else
            cnt <= (cnt == N - 1) ? 4'd0 : cnt + 4'd1; // 计算下一个计数值
    end

endmodule
