module Component_Binary_To_7Segment(
    input             clk,    // 时钟信号
    input             rst_n,  // 复位信号（低电平有效）
    input      [31:0] code,   // 8421BCD码输入
    output     [6:0] seg,    // 段选信号输出（LEDA~LEDG）
    output reg [7:0] cat    // 管选信号输出（SEL0~SEL7）
);

  reg [2:0] cnt;  // 计数器
  reg [3:0] decoder_i;  // 数码管解码输入

  always @(posedge clk or posedge rst_n) begin
    if (rst_n) begin  // 复位信号为低电平时
      decoder_i <= 4'b0;  // 数码管解码输入置零
      cat      <= 8'b11111111;  // 管选信号置全高电平
      cnt       <= 3'b0;  // 计数器复位为0
    end else begin
      case (cnt)
        0: begin
          cat      <= 8'b11111110;  // 第一个管选信号（SEL0）为低电平，选择第一个数码管
          decoder_i <= code[3:0];  // 数码管解码输入为对应的8421BCD码
        end
        1: begin
          cat      <= 8'b11111101;  // 第二个管选信号（SEL1）为低电平，选择第二个数码管
          decoder_i <= code[7:4];  // 数码管解码输入为对应的8421BCD码
        end
        2: begin
          cat      <= 8'b11111011;  // 第三个管选信号（SEL2）为低电平，选择第三个数码管
          decoder_i <= code[11:8];  // 数码管解码输入为对应的8421BCD码
        end
        3: begin
          cat      <= 8'b11110111;  // 第四个管选信号（SEL3）为低电平，选择第四个数码管
          decoder_i <= code[15:12];  // 数码管解码输入为对应的8421BCD码
        end
        4: begin
          cat      <= 8'b11101111;  // 第五个管选信号（SEL4）为低电平，选择第五个数码管
          decoder_i <= code[19:16];  // 数码管解码输入为对应的8421BCD码
        end
        5: begin
          cat      <= 8'b11011111;  // 第六个管选信号（SEL5）为低电平，选择第六个数码管
          decoder_i <= code[23:20];  // 数码管解码输入为对应的8421BCD码
        end
        6: begin
          cat      <= 8'b10111111;  // 第七个管选信号（SEL6）为低电平，选择第七个数码管
          decoder_i <= code[27:24];  // 数码管解码输入为对应的8421BCD码
        end
        7: begin
          cat      <= 8'b01111111;  // 第八个管选信号（SEL7）为低电平，选择第八个数码管
          decoder_i <= code[31:28];  // 数码管解码输入为对应的8421BCD码
        end
      endcase

      if (cnt == 3'h7) cnt <= 3'b0;  // 计数器达到最大值时，复位为0
      else cnt <= cnt + 1'b1;  // 计数器递增
    end
  end

  nixie_cat_decoder unit1 (
      .decoder_i(decoder_i),  // 数码管解码输入
      .decoder_o(seg)         // 数码管段选信号输出
  );
endmodule


// 数码管解码模块
module nixie_cat_decoder (
    input      [3:0] decoder_i,  // 数码管解码输入
    output reg [6:0] decoder_o   // 数码管解码输出
);

  always @(decoder_i) begin
    case (decoder_i)
      4'h0: decoder_o <= 7'h3f;  // 8421BCD码为0时，输出数码管段选信号（LEDA~LEDG）使显示为数字0
      4'h1: decoder_o <= 7'h06;  // 8421BCD码为1时，输出数码管段选信号（LEDA~LEDG）使显示为数字1
      4'h2: decoder_o <= 7'h5b;  // 8421BCD码为2时，输出数码管段选信号（LEDA~LEDG）使显示为数字2
      4'h3: decoder_o <= 7'h4f;  // 8421BCD码为3时，输出数码管段选信号（LEDA~LEDG）使显示为数字3
      4'h4: decoder_o <= 7'h66;  // 8421BCD码为4时，输出数码管段选信号（LEDA~LEDG）使显示为数字4
      4'h5: decoder_o <= 7'h6d;  // 8421BCD码为5时，输出数码管段选信号（LEDA~LEDG）使显示为数字5
      4'h6: decoder_o <= 7'h7d;  // 8421BCD码为6时，输出数码管段选信号（LEDA~LEDG）使显示为数字6
      4'h7: decoder_o <= 7'h07;  // 8421BCD码为7时，输出数码管段选信号（LEDA~LEDG）使显示为数字7
      4'h8: decoder_o <= 7'h7f;  // 8421BCD码为8时，输出数码管段选信号（LEDA~LEDG）使显示为数字8
      4'h9: decoder_o <= 7'h6f;  // 8421BCD码为9时，输出数码管段选信号（LEDA~LEDG）使显示为数字9
      4'ha: decoder_o <= 7'h73;  // 8421BCD码为A时，输出数码管段选信号（LEDA~LEDG）使显示为字母A
      4'hb: decoder_o <= 7'h7c;  // 8421BCD码为B时，输出数码管段选信号（LEDA~LEDG）使显示为字母B
      4'hc: decoder_o <= 7'h39;  // 8421BCD码为C时，输出数码管段选信号（LEDA~LEDG）使显示为字母C
      4'hd: decoder_o <= 7'h5e;  // 8421BCD码为D时，输出数码管段选信号（LEDA~LEDG）使显示为字母D
      4'he: decoder_o <= 7'h79;  // 8421BCD码为E时，输出数码管段选信号（LEDA~LEDG）使显示为字母E
      4'hf: decoder_o <= 7'h71;  // 8421BCD码为F时，输出数码管段选信号（LEDA~LEDG）使显示为字母F
    endcase
  end
endmodule
