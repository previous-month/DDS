module waveform_changed(
    input clk,
    input rst,
    input clicked,
    output [1:0] waveform_type
);

key_press  u_press_0(
    .clk(clk),
    .rst(rst),
    .sw_in_n(clicked),
    .sw_out_n(clicked_process)
);

reg [1:0] waveform_type_temp;
//被处理后的信号
wire clicked_process;

always @(posedge clicked_process or posedge rst)
begin
    if (rst)
        waveform_type_temp <= 2'b00;
    else
    begin
        if(waveform_type_temp == 2'b11)
            waveform_type_temp <= 2'b00;
        else
            waveform_type_temp <= waveform_type_temp + 2'b1;
   end
end

assign waveform_type = waveform_type_temp;

endmodule