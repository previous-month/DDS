module NDivider_Even(
    input clk,        // 输入时钟信号
    input [15:0] N,   // 分频比
    input reset,      // 复位信号
    output wire clk_out // 输出分频后的时钟信号
);
    reg [15:0] counter;// 32位计数器
    reg temp_out;//临时输出

    // 时钟分频逻辑
    always @(posedge clk or posedge reset)
    begin
        if(reset)
	        begin
                counter <= 0;
                temp_out <= 0;
            end
        else  if(counter == N / 2- 1)  
            begin 
                temp_out <= ~temp_out; 
                counter <= 0; 
            end
        else
                counter <= counter + 1'b1;
    end

    assign clk_out = temp_out;

endmodule
