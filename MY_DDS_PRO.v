module MY_DDS_PRO
#(parameter WIDTH=12,
  parameter WIDTH_PHASE=10)
(
    input clk,       // 时钟信号
    input rst,     // 复位信号
    input switch_dds,  // DDS开关

    input btn_wave, //切换波形
    input switch_wave, //切换当前通道的波形
    input switch_channel, //切换两通道频率值
    input switch_parameter, //切换频率或者相位
    //矩阵键盘信号
    input [3:0] row, //行信号
    output [3:0] col,

    //tlv5638输出信号
    output wire cs_o,   // 选通信号
    output wire data_o, // 串行数字信号
    output wire dclock_o, // 串行时钟
    //数码管段选信号
    output [6:0] seg,
    //数码管位选信号
    output [7:0] cat,

    output [15:0] led //按下按键时显示按下次序LED
);

    localparam States_START= 1'b0,States_BUSY = 1'b1;

    // 内部信号声明
    reg start_da = 0; // 初始化为低电平
    wire eoc_da;

    //记录当前输入tlv5638的数据
    reg [15:0] data;
    reg [15:0] data_a;
    reg [15:0] data_b;
    
    reg [1:0] channel;
    reg [1:0] cnt;

    reg  state; // 表示当前传输状态
    reg state_ab; // 表示两位数据的传输状态

    wire [1:0] waveforms; //表示当前波形

    wire [7:0] o_fre_1;
    wire [7:0] o_fre_2;
    wire [WIDTH_PHASE-1:0] o_phase_1;
    wire [WIDTH_PHASE-1:0] o_phase_2;

    wire [WIDTH-1:0] dds_output1; // DDS输出数据1,对应A口
    wire [WIDTH-1:0] dds_output2; // DDS输出数据2,对应B口
    wire dds_output_enable; // 用于启用DDS输出
    
    wire clk_i; // 分频时钟，用于控制DAC
    reg clk_dds=1'b0; // DDS时钟，用于控制DDS
    wire rst_dds; // 用于改变参数时复位dds
    reg eoc_done; //延时记录eoc的状态

    //对输入时钟进行适当分频
    NDivider_Even n_divider_inst_0(.clk(clk) , .N(16'd2) , .reset(rst) , .clk_out(clk_i));

    //DDS模块
    dds #(WIDTH,WIDTH_PHASE) u_dds_1 (
        .clk(clk_dds), // 时钟信号
        .rstn(rst_dds | rst), // 复位信号
        .wave_en(switch_dds), // 启用DDS生成
        .wave_sel(waveforms & switch_wave), // 波形选择，这里选择合适的波形
        .wave_amp(2'b01), // 波形幅度控制，这里选择合适的幅度
        .phase_init(o_phase_1), // 初始相位，这里选择合适的初始相位
        .f_word(o_fre_1), // 频率控制字，这里选择合适的频率
        .dout(dds_output1), // DDS输出数据
        .dout_en(dds_output_enable) // 启用DDS输出
    );

    dds #(WIDTH,WIDTH_PHASE) u_dds_2 (
        .clk(clk_dds), // 时钟信号
        .rstn(rst_dds | rst), // 复位信号
        .wave_en(switch_dds), // 启用DDS生成
        .wave_sel(waveforms & ~switch_wave), // 波形选择，这里选择合适的波形
        .wave_amp(2'b01), // 波形幅度控制，这里选择合适的幅度
        .phase_init(o_phase_2), // 初始相位，这里选择合适的初始相位
        .f_word(o_fre_2), // 频率控制字，这里选择合适的频率
        .dout(dds_output2), // DDS输出数据
        .dout_en(dds_output_enable) // 启用DDS输出
    );

    // TLV5638
    tlv5638 u_tlv5638 (
        .cs_o(cs_o),
        .data_o(data_o),
        .dclock_o(dclock_o),
        .reset_i(rst),
        .clk_i(clk_i),
        .start_i(start_da),
        .eoc_o(eoc_da),
        .data_i(data)
    );

    //切换波形模块
    waveform_changed u_waveform_changed(
        .clk(clk),
        .rst(rst),
        .clicked(btn_wave),
        .waveform_type(waveforms)
    );


    parameter_wave #(WIDTH_PHASE) u_parameter_wave(
        .clk(clk),
        .rst(rst),
        .switch_channel(switch_channel), // 选择通道进行更改
        .switch_parameter(switch_parameter), // 选择频率或者相位进行更改
        //矩阵键盘控制信号
        .row(row),
        .col(col),

        //数码管相关信号
        .cat(cat),
        .seg(seg),

        .o_fre_1(o_fre_1),
        .o_fre_2(o_fre_2),
        .o_phase_1(o_phase_1),
        .o_phase_2(o_phase_2),
        .rst_dds(rst_dds),
        .led(led)
    );



    //标记eoc_da上升沿的位置
    always @(posedge clk_i or posedge rst)
    begin
        if(rst)
        begin
            eoc_done <= 1'b0;
        end
        else
            eoc_done <= eoc_da;
    end

    always @(posedge clk_i or posedge rst)
    begin
        if(rst)
        begin
            start_da <= 1'b0;
            state <= States_START;
            data_a <= 16'h0000;
            data_b <= 16'h0000;
            state_ab <= 1'b0;
        end
        //上一次数据发送完成时更换当前数据
        else if(eoc_done)
        begin
            data_a <= {4'b1000, dds_output1[11:0]}; // 写入DDS输出到 DAC A 缓冲区
            data_b <= {4'b0001, dds_output2[11:0]}; // 写入DDS输出到 DAC B 缓冲区
            case(state_ab)
                1'b0: // 传输a
                begin
                    data <= data_a;
                    state_ab <= 1'b1;
                end
                1'b1: // 传输b
                begin
                    data <= data_b;
                    state_ab <= 1'b0;
                end
            endcase;
            clk_dds <= ~clk_dds; // 产生DDS时钟
            start_da <= 1'b1; //拉高数据位准备下一次发送
        end
        else
        begin
            case(state)
                //初始化状态
                States_START:
                begin
                    data <= 16'b1101000000000010;
                    start_da <= 1'b1;
                    state <= States_BUSY; // 进入发送状态
                end
                //发送状态
                States_BUSY:
                    start_da <= 1'b0;
                default:
                    start_da <= 1'b1;
            endcase;
        end
    end

endmodule
