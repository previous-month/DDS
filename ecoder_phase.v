module ecoder_phase(
    input [3:0] number, //输入键盘值
    output [3:0] number_decoded //输出实际的值
);

reg [3:0] number_decoded_temp;

assign number_decoded = number_decoded_temp;

always@(*) begin
    case(number)
        4'd3: number_decoded_temp <= 4'd1;
        4'd7: number_decoded_temp <= 4'd2;
        4'd11: number_decoded_temp <= 4'd3;
        4'd2: number_decoded_temp <= 4'd4;
        4'd6: number_decoded_temp <= 4'd5;
        4'd10: number_decoded_temp <= 4'd6;
        4'd1: number_decoded_temp <= 4'd7;
        4'd5: number_decoded_temp <= 4'd8;
        4'd9: number_decoded_temp <= 4'd9;
        4'd0: number_decoded_temp <= 4'd12;
        4'd4: number_decoded_temp <= 4'd11;
        4'd8: number_decoded_temp <= 4'd10;
        default: number_decoded_temp <= 4'd0;
    endcase
end
endmodule