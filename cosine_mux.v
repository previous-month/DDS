module cosine_mux #(
    parameter PHASE_DATAWIDTH = 10,
    parameter DAC_DATAWIDTH   = 12
) (
    input [PHASE_DATAWIDTH-3:0] phase,
    output reg [DAC_DATAWIDTH-2:0] data  // = cos(phase)
);
  always @(*) begin
    case (phase)
      // 6'd0: data = 2047;
      // 6'd1: data = 2045; 
      // 6'd2: data = 2042; 
      // 6'd3: data = 2038; 
      // 6'd4: data = 2032; 
      // 6'd5: data = 2025; 
      // 6'd6: data = 2017; 
      // 6'd7: data = 2008; 
      // 6'd8: data = 1998; 
      // 6'd9: data = 1986; 
      // 6'd10: data = 1973;
      // 6'd11: data = 1959;
      // 6'd12: data = 1944;
      // 6'd13: data = 1928;
      // 6'd14: data = 1910;
      // 6'd15: data = 1892;
      // 6'd16: data = 1872;
      // 6'd17: data = 1851;
      // 6'd18: data = 1829;
      // 6'd19: data = 1806;
      // 6'd20: data = 1781;
      // 6'd21: data = 1756;
      // 6'd22: data = 1730;
      // 6'd23: data = 1702;
      // 6'd24: data = 1674;
      // 6'd25: data = 1644;
      // 6'd26: data = 1614;
      // 6'd27: data = 1583;
      // 6'd28: data = 1550;
      // 6'd29: data = 1517;
      // 6'd30: data = 1483;
      // 6'd31: data = 1448;
      // 6'd32: data = 1412;
      // 6'd33: data = 1375;
      // 6'd34: data = 1337;
      // 6'd35: data = 1299;
      // 6'd36: data = 1259;
      // 6'd37: data = 1219;
      // 6'd38: data = 1179;
      // 6'd39: data = 1137;
      // 6'd40: data = 1095;
      // 6'd41: data = 1052;
      // 6'd42: data = 1009;
      // 6'd43: data = 965;
      // 6'd44: data = 920;
      // 6'd45: data = 875;
      // 6'd46: data = 829;
      // 6'd47: data = 783;
      // 6'd48: data = 737;
      // 6'd49: data = 689;
      // 6'd50: data = 642;
      // 6'd51: data = 594;
      // 6'd52: data = 546;
      // 6'd53: data = 497;
      // 6'd54: data = 448;
      // 6'd55: data = 399;
      // 6'd56: data = 350;
      // 6'd57: data = 300;
      // 6'd58: data = 250;
      // 6'd59: data = 200;
      // 6'd60: data = 150;
      // 6'd61: data = 100;
      // 6'd62: data = 50;
      // 6'd63: data = 0;
      8'd0: data = 2047;
8'd1: data = 2047;
8'd2: data = 2047;
8'd3: data = 2047;
8'd4: data = 2047;
8'd5: data = 2046;
8'd6: data = 2046;
8'd7: data = 2045;
8'd8: data = 2044;
8'd9: data = 2044;
8'd10: data = 2043;
8'd11: data = 2042;
8'd12: data = 2041;
8'd13: data = 2040;
8'd14: data = 2039;
8'd15: data = 2038;
8'd16: data = 2036;
8'd17: data = 2035;
8'd18: data = 2034;
8'd19: data = 2032;
8'd20: data = 2031;
8'd21: data = 2029;
8'd22: data = 2027;
8'd23: data = 2025;
8'd24: data = 2023;
8'd25: data = 2021;
8'd26: data = 2019;
8'd27: data = 2017;
8'd28: data = 2015;
8'd29: data = 2013;
8'd30: data = 2011;
8'd31: data = 2008;
8'd32: data = 2006;
8'd33: data = 2003;
8'd34: data = 2000;
8'd35: data = 1998;
8'd36: data = 1995;
8'd37: data = 1992;
8'd38: data = 1989;
8'd39: data = 1986;
8'd40: data = 1983;
8'd41: data = 1980;
8'd42: data = 1977;
8'd43: data = 1973;
8'd44: data = 1970;
8'd45: data = 1966;
8'd46: data = 1963;
8'd47: data = 1959;
8'd48: data = 1956;
8'd49: data = 1952;
8'd50: data = 1948;
8'd51: data = 1944;
8'd52: data = 1940;
8'd53: data = 1936;
8'd54: data = 1932;
8'd55: data = 1928;
8'd56: data = 1924;
8'd57: data = 1919;
8'd58: data = 1915;
8'd59: data = 1910;
8'd60: data = 1906;
8'd61: data = 1901;
8'd62: data = 1896;
8'd63: data = 1892;
8'd64: data = 1887;
8'd65: data = 1882;
8'd66: data = 1877;
8'd67: data = 1872;
8'd68: data = 1867;
8'd69: data = 1861;
8'd70: data = 1856;
8'd71: data = 1851;
8'd72: data = 1845;
8'd73: data = 1840;
8'd74: data = 1834;
8'd75: data = 1829;
8'd76: data = 1823;
8'd77: data = 1817;
8'd78: data = 1812;
8'd79: data = 1806;
8'd80: data = 1800;
8'd81: data = 1794;
8'd82: data = 1788;
8'd83: data = 1781;
8'd84: data = 1775;
8'd85: data = 1769;
8'd86: data = 1763;
8'd87: data = 1756;
8'd88: data = 1750;
8'd89: data = 1743;
8'd90: data = 1736;
8'd91: data = 1730;
8'd92: data = 1723;
8'd93: data = 1716;
8'd94: data = 1709;
8'd95: data = 1702;
8'd96: data = 1695;
8'd97: data = 1688;
8'd98: data = 1681;
8'd99: data = 1674;
8'd100: data = 1667;
8'd101: data = 1659;
8'd102: data = 1652;
8'd103: data = 1644;
8'd104: data = 1637;
8'd105: data = 1629;
8'd106: data = 1622;
8'd107: data = 1614;
8'd108: data = 1606;
8'd109: data = 1598;
8'd110: data = 1591;
8'd111: data = 1583;
8'd112: data = 1575;
8'd113: data = 1567;
8'd114: data = 1558;
8'd115: data = 1550;
8'd116: data = 1542;
8'd117: data = 1534;
8'd118: data = 1525;
8'd119: data = 1517;
8'd120: data = 1509;
8'd121: data = 1500;
8'd122: data = 1491;
8'd123: data = 1483;
8'd124: data = 1474;
8'd125: data = 1465;
8'd126: data = 1457;
8'd127: data = 1448;
8'd128: data = 1439;
8'd129: data = 1430;
8'd130: data = 1421;
8'd131: data = 1412;
8'd132: data = 1403;
8'd133: data = 1393;
8'd134: data = 1384;
8'd135: data = 1375;
8'd136: data = 1366;
8'd137: data = 1356;
8'd138: data = 1347;
8'd139: data = 1337;
8'd140: data = 1328;
8'd141: data = 1318;
8'd142: data = 1308;
8'd143: data = 1299;
8'd144: data = 1289;
8'd145: data = 1279;
8'd146: data = 1269;
8'd147: data = 1259;
8'd148: data = 1250;
8'd149: data = 1240;
8'd150: data = 1230;
8'd151: data = 1219;
8'd152: data = 1209;
8'd153: data = 1199;
8'd154: data = 1189;
8'd155: data = 1179;
8'd156: data = 1168;
8'd157: data = 1158;
8'd158: data = 1148;
8'd159: data = 1137;
8'd160: data = 1127;
8'd161: data = 1116;
8'd162: data = 1106;
8'd163: data = 1095;
8'd164: data = 1085;
8'd165: data = 1074;
8'd166: data = 1063;
8'd167: data = 1052;
8'd168: data = 1042;
8'd169: data = 1031;
8'd170: data = 1020;
8'd171: data = 1009;
8'd172: data = 998;
8'd173: data = 987;
8'd174: data = 976;
8'd175: data = 965;
8'd176: data = 954;
8'd177: data = 943;
8'd178: data = 932;
8'd179: data = 920;
8'd180: data = 909;
8'd181: data = 898;
8'd182: data = 886;
8'd183: data = 875;
8'd184: data = 864;
8'd185: data = 852;
8'd186: data = 841;
8'd187: data = 829;
8'd188: data = 818;
8'd189: data = 806;
8'd190: data = 795;
8'd191: data = 783;
8'd192: data = 772;
8'd193: data = 760;
8'd194: data = 748;
8'd195: data = 737;
8'd196: data = 725;
8'd197: data = 713;
8'd198: data = 701;
8'd199: data = 689;
8'd200: data = 678;
8'd201: data = 666;
8'd202: data = 654;
8'd203: data = 642;
8'd204: data = 630;
8'd205: data = 618;
8'd206: data = 606;
8'd207: data = 594;
8'd208: data = 582;
8'd209: data = 570;
8'd210: data = 558;
8'd211: data = 546;
8'd212: data = 534;
8'd213: data = 521;
8'd214: data = 509;
8'd215: data = 497;
8'd216: data = 485;
8'd217: data = 473;
8'd218: data = 460;
8'd219: data = 448;
8'd220: data = 436;
8'd221: data = 424;
8'd222: data = 411;
8'd223: data = 399;
8'd224: data = 387;
8'd225: data = 374;
8'd226: data = 362;
8'd227: data = 350;
8'd228: data = 337;
8'd229: data = 325;
8'd230: data = 312;
8'd231: data = 300;
8'd232: data = 288;
8'd233: data = 275;
8'd234: data = 263;
8'd235: data = 250;
8'd236: data = 238;
8'd237: data = 225;
8'd238: data = 213;
8'd239: data = 200;
8'd240: data = 188;
8'd241: data = 175;
8'd242: data = 163;
8'd243: data = 150;
8'd244: data = 138;
8'd245: data = 125;
8'd246: data = 113;
8'd247: data = 100;
8'd248: data = 87;
8'd249: data = 75;
8'd250: data = 62;
8'd251: data = 50;
8'd252: data = 37;
8'd253: data = 25;
8'd254: data = 12;
8'd255: data = 0;
    endcase
  end
endmodule
