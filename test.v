`include "MY_DDS_PRO.v"
`include "dds.v"
`include "mem.v"
`include "Keypad.v"
`include "key_press.v"
`include "parameter_wave.v"
`include "NDivider_Even.v"
`include "ratio.v"
`include "tlv5638.v"
`include "Counter_N.v"
`include "Component_Binary_To_7Segment.v"
`include "cosine_mux.v"
`include "data_transport.v"
`include "waveform_changed.v"
`include "ecoder_phase.v"
`timescale 1ps/1ps

module test;


    reg [3:0] row;//行信号
    wire [3:0] col;

    reg clk;       // 时钟信号
    reg rst;    // 复位信号
    reg switch_dds;  // DDS开关
    reg btn_wave; // 波形选择开关
    reg switch_channel; //频率选择
    reg switch_parameter; //参数选择
    reg switch_wave; //通道波形选择
    wire cs_o;   // 选通信号
    wire data_o; // 串行数字信号
    wire dclock_o; // 串行时钟
    
    //数码管位序信号
    wire [7:0] cat;
    //数码管位选信号
    wire [6:0] seg;
    wire [15:0] led;

MY_DDS_PRO dds_inst(
    .clk(clk),
    .rst(rst),
    .switch_dds(switch_dds),
    .cs_o(cs_o),
    .data_o(data_o),
    .dclock_o(dclock_o),
    .cat(cat),
    .seg(seg),

    .btn_wave(btn_wave),
    .switch_parameter(switch_parameter),
    .switch_channel(switch_channel),
    .switch_wave(switch_wave),
    .row(row),
    .col(col),
    .led(led)
);

//(1)clk, reset and other constant regs
   initial begin
      clk               = 1'b0 ;
      rst              = 1'b1 ;
      #100 ;
      rst      = 1'b0 ;
      #10 ;
      forever begin
         #5 ;
         clk = ~clk ;   //system clock, 1000Hz
      end
   end // initial begin

//(2)signal setup ;
//    integer      freq_dst        = 2000000 ;     //2MHz
//    integer      phase_coe       = 2;            //1/4 cycle, that is pi/2
   initial begin
      switch_dds = 1'b0;
      #100 ;
      switch_dds = 1'b1;
      switch_channel = 1'b1;
      switch_parameter = 1'b0;
      #500000;
      row = 4'b0001;
   end // initial begin

   initial
   begin
      $dumpfile("test.vcd");
      $dumpvars(0, test);
      #10000000;
      $finish ;
   end

endmodule