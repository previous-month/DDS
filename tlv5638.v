module tlv5638(
   output cs_o,        // 选通输出
   output data_o,      // 数据输出
   output dclock_o,    // 数据时钟输出
   input reset_i,      // 外部复位输入
   input clk_i,        // 系统时钟输入
   input start_i,      // 启动信号输入
   output eoc_o,       // 转换结束信号输出
   input [15:0] data_i // 数据输入
);

   // 内部信号声明
   parameter [1:0] States_IDEL = 2'b00, States_BUSY = 2'b01;
   reg [1:0] state;

   reg reset_int;
   reg [15:0] data_out = 16'd0;
   reg [3:0] data_out_addr;

   reg eoc_tmp;
   reg cs_o_tmp;
   reg dclock_o_tmp;
   reg data_o_tmp;
   reg [4:0] cnt_wr;
   reg [0:0] t;

   // 状态机
   always @(negedge clk_i or posedge reset_i or posedge eoc_tmp)//_tmp
   begin
      if (reset_i == 1'b1 | eoc_tmp == 1'b1)//_tmp
         state <= States_IDEL;
      else
      begin
         if (start_i == 1'b1 & state == States_IDEL)
         begin
            state <= States_BUSY;
            data_out <= data_i;
         end
      end
   end

   // 复位控制
   always @(negedge clk_i or posedge reset_i)
   begin
      if (reset_i == 1'b1)
         reset_int <= 1'b0;
      else
      begin
         if (eoc_tmp == 1'b1)//_tmp
            reset_int <= 1'b1;
         else
            reset_int <= 1'b0;
      end
   end

   // 数据发送
   always @(posedge clk_i)
   begin
      if (reset_i == 1'b1 | reset_int == 1'b1)
      begin
         cs_o_tmp <= 1'b1;
         cnt_wr = 5'b0;
         t = 1'b0;
         dclock_o_tmp <= 1'b0;
         eoc_tmp <= 1'b0;
         data_out_addr <= 4'b1111;
         data_o_tmp <= 1'b0;
      end
      else
      begin
         if (state == States_BUSY)
         begin
            if (cnt_wr < 5'b10000)
            begin
               eoc_tmp <= 1'b0;
               cs_o_tmp <= 1'b0;
               if (t == 1'b0)
               begin
                  data_o_tmp <= data_out[data_out_addr];
                  dclock_o_tmp <= 1'b1;
                  if (data_out_addr > 4'b0000)
                     data_out_addr <= data_out_addr - 1;
               end
               else
               begin
                  dclock_o_tmp <= 1'b0;
                  cnt_wr = cnt_wr + 1;
               end
               if (t < 1'b1)
                  t = t + 1'b1;
               else
                  t = 1'b0;
            end
            else
            begin
               cs_o_tmp <= 1'b1;
               eoc_tmp <= 1'b1;
            end
         end
      end
   end

   // 输出端口赋值
   assign eoc_o = eoc_tmp;
   assign cs_o = cs_o_tmp;
   assign dclock_o = dclock_o_tmp;
   assign data_o = data_o_tmp;

endmodule
