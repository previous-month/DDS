module Keypad (
    input CLK,
    input RST,
    input [3:0] ROW,
    output reg [3:0] COL,
    
    output reg [3:0] DATA_NUMBER, 
    output reg PRESSED
);

    reg [15:0] DATA;
    reg [1:0] ColSel;

    // 列扫描计数
    always @(posedge CLK or posedge RST) begin
        if (RST) begin
            ColSel <= 2'd0;
            PRESSED <= 0;
        end
        else begin
            ColSel = ColSel + 2'd1;
            if (ColSel == 2'd0) begin
                PRESSED = (DATA != 16'd0);
            end
            else begin
                PRESSED = 0;
            end
        end
    end

    // 列扫描译码器
    always @(*) begin
        case (ColSel)
            2'd0: COL = 4'b1110;
            2'd1: COL = 4'b1101;
            2'd2: COL = 4'b1011;
            2'd3: COL = 4'b0111;
        endcase
    end

    // 行数据读取
    always @(negedge CLK or posedge RST) begin
        if (RST) begin
            DATA <= 16'd0;
        end
        else begin
            case (ColSel)
                2'd0: DATA[3:0] <= ~ROW;
                2'd1: DATA[7:4] <= ~ROW;
                2'd2: DATA[11:8] <= ~ROW;
                2'd3: DATA[15:12] <= ~ROW;
            endcase
        end
    end

  always @(*) begin
    if (RST) DATA_NUMBER <= 4'b0000;
    else if(DATA[15]==1)  DATA_NUMBER<=4'b1111;
    else if(DATA[14]==1)  DATA_NUMBER<=4'b1110;
    else if(DATA[13]==1)  DATA_NUMBER<=4'b1101;
    else if(DATA[12]==1)  DATA_NUMBER<=4'b1100;
    else if(DATA[11]==1)  DATA_NUMBER<=4'b1011;
    else if(DATA[10]==1)  DATA_NUMBER<=4'b1010;
    else if(DATA[ 9]==1)  DATA_NUMBER<=4'b1001;
    else if(DATA[ 8]==1)  DATA_NUMBER<=4'b1000;
    else if(DATA[ 7]==1)  DATA_NUMBER<=4'b0111;
    else if(DATA[ 6]==1)  DATA_NUMBER<=4'b0110;
    else if(DATA[ 5]==1)  DATA_NUMBER<=4'b0101;
    else if(DATA[ 4]==1)  DATA_NUMBER<=4'b0100;
    else if(DATA[ 3]==1)  DATA_NUMBER<=4'b0011;
    else if(DATA[ 2]==1)  DATA_NUMBER<=4'b0010;
    else if(DATA[ 1]==1)  DATA_NUMBER<=4'b0001;
    else if(DATA[ 0]==1)  DATA_NUMBER<=4'b0000;
    else DATA_NUMBER<=4'b0000;
    //没有按键按下，保持当前键值
  end
endmodule
