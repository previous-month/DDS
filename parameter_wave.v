module parameter_wave
#(parameter WIDTH_PHASE=10)
(
    input clk , //时钟信号
    input rst , //复位信号
    input switch_channel , //需要修改的通道
    input switch_parameter, //切换参数
    input [3:0] row, //行信号
    output [3:0] col, // 列信号

    //数码管位序信号
    output [7:0] cat,
    //数码管位选信号
    output [6:0] seg,

    //输出波形参数
    output [7:0] o_fre_1,
    output [7:0] o_fre_2,
    output [WIDTH_PHASE-1:0] o_phase_1,
    output [WIDTH_PHASE-1:0] o_phase_2,

    output rst_dds, //用于改变参数后复位dds

    output [15:0] led //按下按键时显示按下次序LED
);

reg [15:0] led_temp;
assign led = led_temp;

localparam FREQUENCY_BASIC = 8'd1;//设置默认频率值

wire pressed; //用于记录是否按下按键
wire [3:0] number_pressed;

wire clk_i1; // 分频后的信号

// wire order_pressd; //切换相位位序
// wire data_pressd_add;  //切换相位当前位数字
// wire data_pressd_sub;  //切换相位当前位数字

reg [7:0] o_fre_1_temp;
reg [7:0] o_fre_2_temp;

//频率1十位
reg [3:0] fre_1_0x;
//频率1个位
reg [3:0] fre_1_x0;
//频率2十位
reg [3:0] fre_2_0x;
//频率2个位
reg [3:0] fre_2_x0;
//相位个位
reg [3:0] phase_xx0;
//相位十位
reg [3:0] phase_x0x;
//相位百位
reg [3:0] phase_0xx;
//相位位序状态
reg [1:0] num_state;
reg [3:0] parameter_state;

wire [31:0] code;//存储要显示的数据

wire [3:0] decoder_phase;//被译码后的相位输入值

//产生分频信号控制数码管显示
NDivider_Even n_divider_inst_2(.clk(clk) , .N(16'd500) , .reset(rst) , .clk_out(clk_i1));

Component_Binary_To_7Segment segment_inst(
    .clk(clk_i1),
    .rst_n(rst),
    .code(code),
    .cat(cat),
    .seg(seg)
);

//返回当前按下的矩阵键盘值
data_transport u_data_transport(    
    .clk(clk),
    .rst(rst),
    .row(row),
    .col(col),
    .pressed(pressed),
    .number_pressed(number_pressed)
    );

//返回被解码的相位值
ecoder_phase u_ecoder_phase(
    .number(number_pressed),
    .number_decoded(decoder_phase)
);


always@(posedge pressed or posedge rst) begin
    if(rst)
    begin
        //频率值复位
        fre_1_0x <= 4'd0;
        fre_1_x0 <= 4'd1;
        fre_2_0x <= 4'd0;
        fre_2_x0 <= 4'd1;
        o_fre_1_temp <= 8'd1;
        o_fre_2_temp <= 8'd1;
        //相位值复位
        phase_xx0 <= 4'd0;
        phase_x0x <= 4'd0;
        phase_0xx <= 4'd0;
        phase_data <= 4'd0;
        num_state <= 2'd0;
        parameter_state <= 4'hf;
        led_temp <= 16'd15;
    end
    //此时更改频率
    else if(switch_parameter)
    begin
        parameter_state <= 4'hf;
        //更改通道1
        led_temp <= 16'b1 << number_pressed;//记录按下LED的值
        if(switch_channel)
        begin
            case(number_pressed)
                0,1,2,3,4,5,6,7,8://1~9 
                begin
                    fre_1_0x <= 4'd0;
                    fre_1_x0 <= number_pressed + 4'd1;
                    o_fre_1_temp <= number_pressed+4'd1;
                end
                9,10,11,12,13,14,15://10~16
                begin
                    fre_1_0x <= 4'd1;
                    fre_1_x0 <= number_pressed - 4'd9;
                    o_fre_1_temp <= number_pressed+4'd1;
                end           
            endcase;
        end
        //更改通道2
        else
        begin
            case(number_pressed)
                0,1,2,3,4,5,6,7,8://1 
                begin
                    fre_2_0x <= 4'd0;
                    fre_2_x0 <= number_pressed+ 4'd1;
                    o_fre_2_temp <= number_pressed+4'd1;
                end
                9,10,11,12,13,14,15://11
                begin
                    fre_2_0x <= 4'd1;
                    fre_2_x0 <= number_pressed - 4'd9;
                    o_fre_2_temp <= number_pressed+4'd1;
                end
            endcase;
        end
    end
    //此时更改相位
    else
    begin
        led_temp <= 16'b1 << decoder_phase;
        case(decoder_phase)
            //此时更改当前位数据
            0,1,2,3,4,5,6,7,8,9:
            begin
                if(num_state == 2'd1)
                begin
                    phase_xx0 <= decoder_phase;
                end
                else if(num_state == 2'd2)
                begin
                    phase_x0x <= decoder_phase;
                end
                else if(num_state == 2'd3)
                begin
                    phase_0xx <= decoder_phase;
                end
                else
                begin
                    phase_xx0 <= 4'd9;
                    phase_x0x <= 4'd9;
                    phase_0xx <= 4'd9;
                end
            end
            //此时切换位序
            10,11,12:
            begin
                num_state <= decoder_phase - 4'd9;
            end
            default:
            begin
                phase_xx0 <= 4'd0;
                phase_x0x <= 4'd0;
                phase_0xx <= 4'd0;
            end
        endcase
        phase_data <= phase_0xx * 10'd100 + phase_x0x * 10'd10 + phase_xx0;
        parameter_state <= num_state;
    end
end

//拼凑要展示的数据
assign code = {fre_1_0x,fre_1_x0,fre_2_0x,fre_2_x0,phase_0xx,phase_x0x,phase_xx0,parameter_state};

reg [WIDTH_PHASE-1:0] phase_data; // 用于控制dds的相位

//连接dds频率端口
assign o_fre_1 = o_fre_1_temp;
assign o_fre_2 = o_fre_2_temp;

//连接dds相位端口
assign o_phase_1 = 8'd0;
assign o_phase_2 = phase_data;

assign rst_dds = pressed;//| (phase_data_add) | (phase_data_sub);


//遗弃代码
//用于矩阵键盘控制频率
// always @(posedge pressed or posedge rst) begin
//     if(rst)
//     begin
//         fre_data <= 4'd0;
//         o_fre_1_temp <= FREQUENCY_BASIC;
//         o_fre_2_temp <= FREQUENCY_BASIC;
//     end
//     else
//     begin
//         //获取当前频率数据
//         case(wave_fre)
//             4'd0: fre_data <= 8'd1 * FREQUENCY_BASIC;
//             4'd1: fre_data <= 8'd2 * FREQUENCY_BASIC; 
//             4'd2: fre_data <= 8'd3 * FREQUENCY_BASIC;
//             4'd3: fre_data <= 8'd4 * FREQUENCY_BASIC;
//             4'd4: fre_data <= 8'd5 * FREQUENCY_BASIC;
//             4'd5: fre_data <= 8'd6 * FREQUENCY_BASIC;
//             4'd6: fre_data <= 8'd7 * FREQUENCY_BASIC;
//             4'd7: fre_data <= 8'd8 * FREQUENCY_BASIC;
//             4'd8: fre_data <= 8'd9 * FREQUENCY_BASIC;
//             4'd9: fre_data <= 8'd10 * FREQUENCY_BASIC;
//             4'd10: fre_data <= 8'd11 * FREQUENCY_BASIC;
//             4'd11: fre_data <= 8'd12 * FREQUENCY_BASIC;
//             4'd12: fre_data <= 8'd13 * FREQUENCY_BASIC;
//             4'd13: fre_data <= 8'd14 * FREQUENCY_BASIC;
//             4'd14: fre_data <= 8'd15 * FREQUENCY_BASIC;
//             4'd15: fre_data <= 8'd16 * FREQUENCY_BASIC;
//             default: fre_data <= FREQUENCY_BASIC;
//         endcase;
//         //选择通道进行赋值
//         if(switch_channel == 1'd0)
//             o_fre_1_temp <= fre_data * FREQUENCY_BASIC;
//         else
//             o_fre_2_temp <= fre_data * FREQUENCY_BASIC;
//     end
// end
// //消抖模块
// key_press u_key_press_1(
//     .clk(clk),
//     .rst(rst),
//     .sw_in_n(phase_order),
//     .sw_out_n(order_pressd)
// );
// key_press u_key_press_2(
//     .clk(clk),
//     .rst(rst),
//     .sw_in_n(phase_data_add),
//     .sw_out_n(data_pressd_add)
// );
// key_press u_key_press_3(
//     .clk(clk),
//     .rst(rst),
//     .sw_in_n(phase_data_sub),
//     .sw_out_n(data_pressd_sub)
// );
// reg order; // 用于控制当前加减的数据大小
// reg add_temp; // 记录处于递增状态
// reg sub_temp; // 记录处于递减状态

// //表示从个位到百位进行切换
// always @(posedge phase_order or posedge rst) begin
//     if(rst) 
//     begin
//         order <= 1'd0;
//     end
//     else
//     begin
//         case(order)
//             1'd0: order <= 1'd1;
//             1'd1: order <= 1'd0;
//             default: order <= 1'd0;
//         endcase;
//     end
// end

// //获取当前按键状态
// always @(posedge clk or posedge rst) begin
//     if(rst)
//     begin
//         add_temp <= 1'd0;
//     end
//     else
//     begin
//         add_temp <= phase_data_add;
//     end
// end
// always @(posedge clk or posedge rst) begin
//     if(rst)
//     begin
//         sub_temp <= 1'd0;
//     end
//     else
//     begin
//         sub_temp <= phase_data_sub;
//     end
// end

// //改变当前相位数据
// always @(posedge clk or posedge rst) begin
//     if(rst)
//     begin
//         phase_data <= 8'd0;
//     end
//     else if(add_temp == 1'd1)
//     begin
//         case(order)
//             1'd0: 
//             begin
//                 if(phase_data + 8'd1 <= 8'd128)
//                    phase_data <= phase_data + 1'd1;
//                 else
//                    phase_data <= 8'd0;//超出范围时自动清零
//             end
//             1'd1:
//             begin
//                 if(phase_data + 8'd10 <= 8'd128)
//                     phase_data <= phase_data + 8'd10;
//                 else
//                     phase_data <= 8'd0;//超出范围时自动清零
//             end
//             default: phase_data <= phase_data;
//         endcase;
//     end
//     else if(sub_temp == 1'd1)
//     begin
//         case(order)
//             1'd0:
//             begin
//                 if(phase_data - 8'd1 >= 8'd0)
//                    phase_data <= phase_data - 8'd1;
//                 else
//                    phase_data <= 8'd128;
//             end
//             1'd1:
//             begin
//                 if(phase_data - 8'd10 >= 8'd0)
//                     phase_data <= phase_data - 8'd10;
//                 else
//                     phase_data <= 8'd128;
//             end
//             default: phase_data <= phase_data;
//         endcase;
//     end
// end

endmodule
