module data_transport (
    input clk,
    input rst,
    input [3:0] row,
    output [3:0] col,
    output pressed,
    output [3:0] number_pressed
);

    wire [3:0] DATA_NUMBER;
    wire pressed_temp;
    assign number_pressed = DATA_NUMBER;

wire clk_i2; // 分频后的信号
NDivider_Even n_divider_inst_1(.clk(clk) , .N(16'd65534) , .reset(rst) , .clk_out(clk_i2));

Keypad u_Keypad(
    .CLK(clk_i2),
    .RST(rst),
    .ROW(row),
    .COL(col),
    .DATA_NUMBER(DATA_NUMBER),
    .PRESSED(pressed_temp)
);

assign pressed = pressed_temp;

endmodule